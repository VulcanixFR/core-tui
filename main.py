from src.core_client import Core_unix_client, Core_client_error
from src.app_ui import Core_terminal
from threading import Thread, Event
from time import sleep
from src.getchar import getch

sock = Core_unix_client()
term = Core_terminal()
render_flag = Event()

# Handles the packets and triggers UI updates 
def handle_packet (msg):

    if msg["type"] == "vitals":
        term.vitals(msg["payload"])
        return
    
    if msg['type'] == "vitals_auto":
        term.vitals_load(msg["payload"], True)
        return
    
    if msg["type"] == "help":
        term.help(msg["payload"])
        return
    
    if msg["type"] == "logo":
        term.print(msg["payload"])
        return

    term.print(f'{term.fg256(205)}[{msg["type"]}] {msg["payload"]}{term.colour_reset()}')
    
def update_display():

    while sock.is_opened():
        # Allow the render to be paused
        render_flag.wait()

        # Reading the socket's message queue
        msg = sock.data()

        while msg is not None:
            # Detects wether we have a string (raw packet) or a JSON one
            if type(msg) == str:
                term.print(msg)
            else:
                handle_packet(msg)

            # Not forgetting the end of the loop
            msg = sock.data()

        sleep(0.1)

def exec_cmd (cmd):
    if cmd == "":
        pass

    elif cmd == "clear":
        term.clear()
        term.bandeau()

    elif cmd == "exit":
        sock.close()
        exit(0)            

    else:
        term.sending(cmd)
        sock.send(cmd)

    sleep(0.1)

def handle_escape_code (esc):
    a = esc[0]
    b = esc[1]

    if a != 91: # Not an arrow
        return

    if b == 65: # Up Arrow
        if term.cmd_history_pos == 0:
            return
        
        term.cmd_history_pos -= 1
        term.buffer = term.cmd_history[term.cmd_history_pos]
        term.bandeau()

    if b == 66: # Down Arrow
        if term.cmd_history_pos == len(term.cmd_history):
            return
        
        term.cmd_history_pos += 1
        if term.cmd_history_pos == len(term.cmd_history):
            term.buffer = ""
        else:
            term.buffer = term.cmd_history[term.cmd_history_pos]
        
        term.bandeau()



sock.open("../core/_sock")
term.clear()

render_flag.set()
render_thread = Thread(daemon=True, target=update_display)
render_thread.start()

sock.upgrade()
sleep(0.1)
sock.set_id("vulcanix")
sleep(0.1)

char = ord(getch())
escape = False
escape_buffer = []

while char != 0 and sock.is_opened():

    if escape:
        escape_buffer.append(char)
        if len(escape_buffer) > 1:
            handle_escape_code(escape_buffer)
            escape = False

    elif char == 3:       # Ctrl-C
        sock.close()
        exit(0)

    elif char == 13:    # Enter
        exec_cmd(term.buffer)
        term.buffer = ""
        term.clear_line()
        term.cmd_history_pos = len(term.cmd_history)

    elif char == ord(":"):
        render_flag.clear()
        cmd = term.get_input()
        term.clear_line()
        render_flag.set()
        term.cmd_history.append(cmd)
        term.cmd_history_pos = len(term.cmd_history)
        exec_cmd(cmd)

    elif char == 27:    # Ascii Escape code
        escape = True
        escape_buffer = []

    # else:
    #     term.print(char)

    char = ord(getch())