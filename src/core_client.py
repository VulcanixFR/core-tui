from threading import Thread
import socket
import json
from typing import Any
from uuid import uuid4
from time import sleep

class Core_unix_client (Thread):

    id = ""

    _opened = False
    _raw = True
    _message_queue = []

    def __init__ (self):
        super().__init__(daemon=True)
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        pass

    def open (self, path: str):
        self.socket.connect(path)
        self._opened = True
        self.start()
        pass

    def close (self):
        if not self._opened:
            return
        
        self.socket.close()
        self._opened = False

    def upgrade (self):
        if not self._opened:
            raise Core_client_error("Socket not opened")
        
        self._raw = False
        self.socket.send("@upgrade".encode("utf-8"))

    def set_id (self, id: str):
        if not self._opened:
            raise Core_client_error("Socket not opened")
        
        self.id = id
        self.socket.send(("@id " + id).encode("utf-8"))

    def send (self, payload: str, type: str = "request"):
        if self._raw == True:
            self.socket.send((payload + "\0").encode("utf-8"))
            return
        
        req = {
            "uid": uuid4().hex,
            "type": type,
            "payload": payload
        }

        msg = json.dumps(req) + "\0"
        self.socket.send(msg.encode("utf-8"))

    def is_raw (self):
        return self._raw
    
    def is_opened (self):
        return self._opened
    
    def has_data (self):
        return len(self._message_queue) > 0
    
    def data (self):
        if len(self._message_queue) == 0:
            return None
        
        return self._message_queue.pop(0)

    def run (self):

        while self._opened:
            try:
                data = self.socket.recv(4 * 1024) # 4ko
                msg = data.decode("utf-8")[:-1]
                

                if self._raw:
                    self._message_queue.append(msg)
                    continue

                obj = json.loads(msg)
                self._message_queue.append(obj)

            except json.JSONDecodeError:
                raise Core_client_error("Can't parse data: " + msg)
            
            except UnicodeDecodeError:
                raise Core_client_error("Recieved invalid buffer")

        pass


class Core_client_error (BaseException):
    pass
    
