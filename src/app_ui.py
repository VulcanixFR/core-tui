import os
import json
from time import sleep

class Core_terminal ():

    buffer = ""
    cmd_history = []
    cmd_history_pos = 0

    def __init__(self):
        self._vitals = None

    def fg (self, r, g = None, b = None):
        if g is None or b is None:
            return self.fg256(r)
        return f"\x1b[38;2;{r};{g};{b}m"

    def bg (self, r, g = None, b = None):
        if g is None or b is None:
            return self.bg256(r)
        return f"\x1b[48;2;{r};{g};{b}m"
    
    def fg256 (self, code):
        return f"\x1b[38;5;{code}m"

    def bg256 (self, code):
        return f"\x1b[48;5;{code}m"

    def colour_reset (self):
        return "\x1b[0m"
    
    def input (self, code: int):

        if code >= 32 and code != 127:
            self.buffer += chr(code)

        elif code == 127:
            end = max(0, len(self.buffer) - 1)
            self.buffer = self.buffer[0:end]
        
        self.moveTo(self.term_size.lines - 2)
        self.clear_line()
        print(self.buffer, end="", flush=True)

    
    def home (self):
        print("\x1b[H")

    def clear (self):
        print("\x1b[2J", end="", flush=True)


    def clear_line (self):
        print("\x1b[2K", end="", flush=True)


    def moveTo (self, y: int, x: int = 0):
        print(f"\x1b[{y};{x}f")


    def bandeau (self):
        self.term_size = os.get_terminal_size()

        self.moveTo(self.term_size.lines - 3)
        self.clear_line()
        c = self.fg(100, 100, 100)
        print(c + "-" * (self.term_size.columns - 1) + self.colour_reset(), end="", flush=True)        
        
        self.moveTo(self.term_size.lines - 2)
        self.clear_line()
        print(self.buffer, end="", flush=True)

        self.moveTo(self.term_size.lines - 1)
        self.clear_line()

        if self._vitals is None:
            print(self.fg(196) + self.bg(233), end="")
            self.clear_line()
            print("NO VITALS", end="", flush=True)
        
        else:
            print(self._mk_bandeau(), end="", flush=True)

        self.moveTo(self.term_size.lines - 2, len(self.buffer))
        print(self.colour_reset(), end="")

    def _mk_bandeau (self) -> str:
        host = f' {self._vitals["os"]["hostname"]} '
        dist = f' {self._vitals["os"]["distro"]} {self._vitals["os"]["release"]} '

        cpu = f' CPU: {((self._vitals["load"] * 10) // 1) / 10}% '
        ram = f' RAM: {((self._vitals["ram"]["use"] * 10) // 1) / 10}% '

        middle = " " * (self.term_size.columns - len(host) - len(dist) - len(cpu) - len(ram))

        out = self.fg(0xcc, 0, 0x77) + self.bg(233) + host
        out += self.fg(251) + self.bg(235) + dist + middle
        out += self.fg(255) + self.bg(52) + cpu
        out += self.bg(22) + ram + self.colour_reset()

        return out
           

    # def input (self, placeholder="> "):
    #     self.term_size = os.get_terminal_size()

    #     self.moveTo(self.term_size.lines - 2)
    #     self.clear_line()

    #     ret = input(placeholder)
    #     # self.moveTo(self.term_size.lines - 4)
    #     # self.clear_line()

    #     return ret


    def print (self, *args):
        self.term_size = os.get_terminal_size()

        self.moveTo(self.term_size.lines - 1)
        self.clear_line()        
        self.moveTo(self.term_size.lines - 2)
        self.clear_line()        
        self.moveTo(self.term_size.lines - 3)
        self.clear_line()
        print(*args, flush=True)
        print("\n")
        self.bandeau()
        

    def help (self, msg):
        try:
            help = json.loads(msg)

            title = f"{help['title']} - {help['descritpion']}"
            lines = list(map(map_line, filter(lambda l: l["type"] == "route", help["lines"]))) 
            lines += list(map(map_line, filter(lambda l: l["type"] == "cmd", help["lines"])))

            self.simple_box(title, lines)

        except json.JSONDecodeError:
            self.print(msg)

    def simple_box (self, title, lines, fg = (255, 255, 255), bg = (0x04, 0x82, 0xff), shadow = (233,)):
        title = f'╡ {title} ╞' 
        lines = list(map(lambda l: f'  {l}  ', lines))
                
        c = self.fg(*fg) + self.bg(*bg)
        sh = self.bg(*shadow) + " "
        nl = self.colour_reset() + "\r\n"

        _width = len(title) + 2
        for l in lines:
            _width = max(len(l), _width)
        
        _half_title_w = (_width - len(title)) // 2
        
        if _half_title_w * 2 < _width:
            _half_title_w += 1

        width = 2 * _half_title_w + len(title) - (1 - len(title) % 2)
        
        buffer = c + "╔" + ( _half_title_w * "═" + title + "═" * (_half_title_w - (1 - len(title) % 2))) + "╗" + nl
        buffer += c + "║" + " " * width + "║" + sh + nl

        for l in lines:
            buffer += c + '║' + l + (" " * (width - len(l))) + "║" + sh + nl

        buffer += c + "║" + " " * width + "║" + sh + nl
        buffer += c + "╚" + "═" * width + "╝" + sh + nl
        buffer += " " + sh + " " * (width + 1) + nl

        self.print(buffer)

    def vitals (self, msg: str):

        data = self.vitals_load(msg)            
        title = data["os"]["hostname"]
        lines = [
            f'OS: {data["os"]["distro"]} {data["os"]["release"]} ({data["os"]["kernel"]})',
            '',
            f'Load: {((data["load"] * 100) // 1) / 100}%',
            f'RAM:  {((data["ram"]["use"] * 100) // 1) / 100}% ({data["ram"]["used"] // 1000000}/{data["ram"]["total"] // 1000000}M)',
            '', 
            "FS:",               
            *list(map(map_volume, data["fs"])),
            "", "USB:",
            *list(map(map_usb, data["usb"])),
            "", "NET:",
            *list(map(map_net, data["net"]))
        ]

        self.simple_box(title, lines, bg=(22,))

    def vitals_load (self, msg, render=False):
        try:
            data = json.loads(msg)
            self._vitals = data

            if render:
                self.bandeau()

            return data

        except json.JSONDecodeError:
            self.print(msg)

    def sending (self, cmd: str):
        self.print(self.fg256(240) + ">>", cmd, self.colour_reset())

    def get_input(self):
        
        self.moveTo(self.term_size.lines - 2)
        self.clear_line()
        
        return input(":")



def map_line (l):
    return f"{l['id']}{'' if len(l['args']) == 0 else ' ' + '  '.join(list(map(map_args, l['args'])))}: {l['description']}"

def map_args (arg):
    return f'<{arg["name"]}>' if arg['required'] else f'[{arg["name"]}]'

def map_volume (v):
    return f' [{v["type"]}] {v["mount"]} {((v["use"] *100) // 1) / 100}% ({v["used"] // 1000000}/{v["size"] // 1000000}M)'

def map_usb (u):
    return f' [{u["bus"]}-{u["deviceId"]}] {u["name"]}'

def map_net (n):
    return f' {n["iface"]}: {n["ip4"]} ({n["ip6"]})'
